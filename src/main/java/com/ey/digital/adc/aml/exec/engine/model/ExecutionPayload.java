package com.ey.digital.adc.aml.exec.engine.model;

import lombok.Data;

import java.util.List;
import java.util.Map;

//import com.ey.digital.adc.aml.model.ExecutionPayload;

@Data
public class ExecutionPayload {

    private Integer id;
    private String actionType;
    private Integer sequenceId;
    private String type;
    private Integer ruleId;
    private boolean getResult;
    private List<String> sqlQueries;
    private Map<String, List<String>> resultData;
    
    public ExecutionPayload() {
    	
    }
    
	public ExecutionPayload(ExecutionPayload executionPayload) {
	     this.id = executionPayload.id;
	     this.actionType = executionPayload.actionType;
	     this.sequenceId = executionPayload.sequenceId;
	     this.type = executionPayload.type;
	     this.getResult = executionPayload.getResult;
	     this.sqlQueries = executionPayload.sqlQueries;
	     this.resultData = executionPayload.resultData;
	     
	  }

}
