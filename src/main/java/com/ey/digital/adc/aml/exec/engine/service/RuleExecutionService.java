package com.ey.digital.adc.aml.exec.engine.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class RuleExecutionService {
	private final static Logger _logger = LoggerFactory.getLogger(RuleExecutionService.class.getName());
	@Autowired
	private JdbcTemplate hiveJdbcTemplate;

	public void execute(String sqlQuery) {
		hiveJdbcTemplate.execute(sqlQuery);
	}

	public Integer executeForResult(String sqlQuery) {
		return hiveJdbcTemplate.queryForObject(sqlQuery, Integer.class);
	}

	public Double getValue(String sql) {
		return this.hiveJdbcTemplate.queryForObject(sql, Double.class);
	}

	public void executeQuery(String sqlQuery) {
		_logger.info("executing the query :" + sqlQuery);
		hiveJdbcTemplate.execute(sqlQuery.toString());
		_logger.info("Completed  query :" + sqlQuery);
		_logger.info(sqlQuery + " statement ran successfully");
	}

	public Map<String, List<String>> executeQry(String sqlQuery) {

		Map<String, List<String>> generateDataQueryresults = new HashMap();
		try {
			List<Map<String, Object>> resultMapList = this.hiveJdbcTemplate.queryForList(sqlQuery);

			for (Map<String, Object> resultMap : resultMapList) {
				List<String> valueList = new ArrayList<String>();
				Double valueDouble = null;
				Long valueLong = null;

				String keyValue = null;
				String strValue = null;
				for (String key : resultMap.keySet()) {
					Object value = resultMap.get(key);
					if (value != null) {
						System.out.println(value.getClass() + "," + value);

						if (value instanceof String) {
							strValue = (String) value;
							if (keyValue == null) {
								keyValue = strValue;
							}
							if (strValue.indexOf(",") != -1) {
								valueList = Arrays.asList(((String) value).replaceAll("[\\[\\]]", "").split("\\,"));

								System.out.println(valueList);
								System.out.println("length " + valueList.size());
							}
						} else if (value instanceof Integer) {
							keyValue = ((Integer) value).toString();
						} else if (value instanceof Long) {
							System.out.println(valueLong);
							valueLong = ((Long) value);
							// valueList = new ArrayList();
							valueList.add(String.valueOf(valueLong));
							System.out.println(valueList);
							System.out.println("length " + valueList.size());
						} else if (value instanceof Double) {
							System.out.println(valueDouble);
							valueDouble = ((Double) value);
							// valueList = new ArrayList();
							valueList.add(String.valueOf(valueDouble));
							System.out.println(valueList);
							System.out.println("length " + valueList.size());
						}

					} else {
						System.out.println("null object");
					}

				} // inner for loop
				if (keyValue != null && valueList != null) {
					generateDataQueryresults.put(keyValue, valueList);
				}
				keyValue = null;

				/*
				 * for (Entry<String, Object> entry: resultMap.entrySet()) { Object value =
				 * entry.getValue(); key = entry.getKey(); System.out.println("key="+key); if
				 * (value != null) { System.out.println(value.getClass() + "," + value); if
				 * (value instanceof String) { strValue = (String)value; if
				 * (strValue.indexOf(",") != -1) { valueList =
				 * Arrays.asList(((String)value).split("\\,")); System.out.println(valueList);
				 * System.out.println("length "+ valueList.size()); } } // String hiveString =
				 * (String) value; // List<String> valueList =
				 * Arrays.asList(hiveString.split(",")); //ObjectMapper oMapper = new
				 * ObjectMapper(); //List<String> approx_percentileList =
				 * oMapper.convertValue(value, List.class); //generateDataQueryresults.put(key,
				 * approx_percentileList);
				 * 
				 * //System.out.println("approx_percentileList=" + approx_percentileList);
				 * //System.out.println("outputMap=" + generateDataQueryresults);
				 * //System.out.println(value.getClass().toString() + ", " + value); } else {
				 * System.out.println("null object"); } }
				 */
			}
		} catch (EmptyResultDataAccessException erdae) {
			_logger.error(erdae.getMessage());
		}
		System.out.println("returning " + generateDataQueryresults);
		return generateDataQueryresults;

	}

	public Map<String, List<String>> executeCategoricalQry(String sqlQuery) {
		Map<String, List<String>> generateDataQueryresults = new HashMap<String, List<String>>();

		// hiveJdbcTemplate.queryForObject(sqlQuery, String.class);
		List<String> distinctValues = hiveJdbcTemplate.query(sqlQuery,
				(rs, rowNum) -> new String(rs.getString("cat_id")));
		generateDataQueryresults.put("Categorical", distinctValues);
		_logger.info("categ " + generateDataQueryresults);
		return generateDataQueryresults;

	}

	public Map<String, List<String>> executeNumericQry(String sqlQuery) {

		Map<String, List<String>> generateDataQueryresults = new HashMap();
		try {
			List<Map<String, Object>> resultMapList = this.hiveJdbcTemplate.queryForList(sqlQuery);

			for (Map<String, Object> resultMap : resultMapList) {
				List<String> valueList = new ArrayList<String>();
				Double valueDouble = null;
				Long valueLong = null;

				String keyValue = null;
				String strValue = null;
				for (String key : resultMap.keySet()) {
					Object value = resultMap.get(key);
					if (value != null) {
						System.out.println(value.getClass() + "," + value);

						if (value instanceof String) {
							strValue = (String) value;
							if (keyValue == null) {
								keyValue = "Numeric";
							}
							if (strValue.indexOf(",") != -1) {
								valueList = Arrays.asList(((String) value).replaceAll("[\\[\\]]", "").split("\\,"));

								System.out.println(valueList);
								System.out.println("length " + valueList.size());
							}
							valueList.add(strValue);

						} else if (value instanceof Integer) {
							keyValue = ((Integer) value).toString();
						} else if (value instanceof Long) {
							System.out.println(valueLong);
							valueLong = ((Long) value);
							// valueList = new ArrayList();
							valueList.add(String.valueOf(valueLong));
							System.out.println(valueList);
							System.out.println("length " + valueList.size());
						} else if (value instanceof Double) {
							System.out.println(valueDouble);
							valueDouble = ((Double) value);
							// valueList = new ArrayList();
							valueList.add(String.valueOf(valueDouble));
							System.out.println(valueList);
							System.out.println("length " + valueList.size());
						}

					} else {
						System.out.println("null object");
					}

				} // inner for loop
				if (keyValue != null && valueList != null) {
					generateDataQueryresults.put(keyValue, valueList);
				}
				keyValue = null;

			}
		} catch (EmptyResultDataAccessException erdae) {
			_logger.error(erdae.getMessage());
		}
		System.out.println("returning " + generateDataQueryresults);
		return generateDataQueryresults;
	}
	
	public Integer execQryForGenStats(String sqlQuery) {
		Map<String, List<String>> generateDataQueryresults = new HashMap();
		try {
			Integer result = this.hiveJdbcTemplate.queryForObject(sqlQuery, Integer.class);
			System.out.println("returning " + generateDataQueryresults);
			return result;

		} catch (EmptyResultDataAccessException erdae) {
			_logger.error(erdae.getMessage());
		}
		return null;
	}

}