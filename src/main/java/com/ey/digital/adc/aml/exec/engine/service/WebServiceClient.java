package com.ey.digital.adc.aml.exec.engine.service;

import com.ey.digital.adc.aml.exec.engine.model.ExecutionPayload;

import org.codehaus.groovy.runtime.powerassert.SourceText;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Component;
import parquet.org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

@Component
public class WebServiceClient {
	private final static Logger _logger = LoggerFactory.getLogger(WebServiceClient.class.getName());

	private String webServiceUrl;

	public ExecutionPayload getExecutableRulesHeader(String token, Integer executionId) throws IOException {
		String url = String.format("%sexecute-rule?executionId=%s", getWebServiceUrl(), executionId);
		System.out.println("url = " + url);
		URL myURL = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
		conn.setRequestProperty("X-Authorization", token);
		conn.setRequestMethod("GET");
		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		ExecutionPayload executionPayload = new ObjectMapper().readValue(rd, ExecutionPayload.class);
		System.out.println("executionPayload = " + executionPayload);
		rd.close();
		return executionPayload;
	}

	public void postResult(ExecutionPayload executionPayload) throws IOException {
		URL url = new URL(webServiceUrl + "execute-rule");
		URLConnection conn;
		conn = url.openConnection();
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("X-Authorization", "token");
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		ObjectMapper mapper = new ObjectMapper();
		String payload = mapper.writeValueAsString(executionPayload);
		wr.write(payload);
		wr.flush();
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		wr.close();
		rd.close();
	}

	public void postGenerateDataResult(List<ExecutionPayload> executionPayloads) throws IOException {

		// _logger.info("postGenerateDataResult 1");
		String thurl = String.format("%sgeneratedata/thresholdqueryresults", getWebServiceUrl());

		_logger.info("postGenerateDataResult url" + thurl);
		// URL url = new URL(thurl);
		URL myURL = new URL(thurl);
		HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
		// URLConnection conn;
		// conn = url.openConnection();

		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("X-Authorization", "token");
		conn.setUseCaches(false);
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		ObjectMapper mapper = new ObjectMapper();
		String payload = mapper.writeValueAsString(executionPayloads);
		// _logger.info("postGenerateDataResult payload:" + "{
		// \"token\":\"82C36AF3-B220-459B-9AAD-88A81883E970\" , \"messages\": " +
		// payload + "}");
		System.out.println("postGenerateDataResult payload:"
				+ "{ \"token\":\"82C36AF3-B220-459B-9AAD-88A81883E970\" , \"messages\": " + payload + "}");
		wr.write("{ \"token\":\"82C36AF3-B220-459B-9AAD-88A81883E970\" , \"messages\": " + payload + "}");
		wr.flush();
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		wr.close();
		rd.close();
	}

	public List<ExecutionPayload> getExecutableGenerateDataHeader(String token, Integer ruleId, Integer rtcid,
			String texecid) throws IOException {
		String url = String.format("%sgeneratedata/getsqls?token=%s&ruleid=%s&rtcid=%s&texecid=%s", getWebServiceUrl(),
				token, ruleId, rtcid, texecid);
		_logger.info("url = " + url);
		URL myURL = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
		conn.setRequestProperty("X-Authorization", token);
		conn.setRequestMethod("GET");
		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		_logger.info("executionPayloads>>>>>> = " + rd.toString());
		ObjectMapper mapper = new ObjectMapper();
		// List<ExecutionPayload> executionPayloads = Arrays.asList( new
		// ObjectMapper().readValue(rd, List<ExecutionPayload.class>));

		List<ExecutionPayload> executionPayloads = mapper.readValue(rd,
				mapper.getTypeFactory().constructCollectionType(List.class, ExecutionPayload.class));

		_logger.info("executionPayloads = " + executionPayloads);
		_logger.info("executionPayloads size >>>>>>= " + executionPayloads.size());
		rd.close();
		return executionPayloads;
	}

	public List<ExecutionPayload> getExecutableThresholdCommitHeader(String token, Integer rtcid) throws IOException {
		String url = String.format("%sthreshold-commit?token=%s&rtcid=%s", getWebServiceUrl(), token, rtcid);
		_logger.info("url = " + url);
		URL myURL = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
		conn.setRequestProperty("X-Authorization", token);
		conn.setRequestMethod("GET");
		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		_logger.info("executionPayloads>>>>>> = " + rd.toString());
		ObjectMapper mapper = new ObjectMapper();
		// List<ExecutionPayload> executionPayloads = Arrays.asList( new
		// ObjectMapper().readValue(rd, List<ExecutionPayload.class>));

		List<ExecutionPayload> executionPayloads = mapper.readValue(rd,
				mapper.getTypeFactory().constructCollectionType(List.class, ExecutionPayload.class));

		_logger.info("executionPayloads = " + executionPayloads);
		_logger.info("executionPayloads size >>>>>>= " + executionPayloads.size());
		rd.close();
		return executionPayloads;
	}

	public void postThresholdCommitResult(List<ExecutionPayload> executionPayloads) throws IOException {
		String thurl = String.format("%sthreshold-commit", getWebServiceUrl());
		URL url = new URL(thurl);
		URLConnection conn;
		conn = url.openConnection();
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("X-Authorization", "token");
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		ObjectMapper mapper = new ObjectMapper();
		String payload = mapper.writeValueAsString(executionPayloads);
		wr.write(payload);
		wr.flush();
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		wr.close();
		rd.close();
	}

	public List<ExecutionPayload> getSegGenerateDataHeader(String token, Integer modelid) throws IOException {
		String url = String.format("%ssegmentationmodel/getsqls?token=%s&modelid=%s", getWebServiceUrl(), token,
				modelid);
		_logger.info("url = " + url);
		URL myURL = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
		conn.setRequestProperty("X-Authorization", token);
		conn.setRequestMethod("GET");
		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		_logger.info("executionPayloads>>>>>> = " + rd.toString());
		ObjectMapper mapper = new ObjectMapper();
		// List<ExecutionPayload> executionPayloads = Arrays.asList( new
		// ObjectMapper().readValue(rd, List<ExecutionPayload.class>));

		List<ExecutionPayload> executionPayloads = mapper.readValue(rd,
				mapper.getTypeFactory().constructCollectionType(List.class, ExecutionPayload.class));

		_logger.info("executionPayloads = " + executionPayloads);
		_logger.info("executionPayloads size >>>>>>= " + executionPayloads.size());
		rd.close();
		return executionPayloads;
	}

	public void postSGGenerateDataResult(List<ExecutionPayload> executionPayloads) throws IOException {

		// _logger.info("postGenerateDataResult 1");
		String thurl = String.format("%ssegmentationmodel/generatedataresults", getWebServiceUrl());

		_logger.info("postGenerateDataResult url" + thurl);
		// URL url = new URL(thurl);
		URL myURL = new URL(thurl);
		HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
		// URLConnection conn;
		// conn = url.openConnection();

		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("X-Authorization", "token");
		conn.setUseCaches(false);
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		ObjectMapper mapper = new ObjectMapper();
		String payload = mapper.writeValueAsString(executionPayloads);
		// _logger.info("postGenerateDataResult payload:" + "{
		// \"token\":\"82C36AF3-B220-459B-9AAD-88A81883E970\" , \"messages\": " +
		// payload + "}");
		System.out.println("postGenerateDataResult payload:"
				+ "{ \"token\":\"82C36AF3-B220-459B-9AAD-88A81883E970\" , \"messages\": " + payload + "}");
		wr.write("{ \"token\":\"82C36AF3-B220-459B-9AAD-88A81883E970\" , \"messages\": " + payload + "}");
		wr.flush();
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		wr.close();
		rd.close();
	}

	public List<ExecutionPayload> getGenStats(String token, String execid) throws IOException {
		String url = String.format("%ssegnodestat/get-genstatsql?token=%s&execid=%s", getWebServiceUrl(), token, execid);
		_logger.info("url = " + url);
		URL myURL = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
		conn.setRequestProperty("X-Authorization", token);
		conn.setRequestMethod("GET");
		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		ObjectMapper mapper = new ObjectMapper();

		List<ExecutionPayload> executionPayloads = mapper.readValue(rd,
				mapper.getTypeFactory().constructCollectionType(List.class, ExecutionPayload.class));
		rd.close();
		return executionPayloads;
	}
	
	public void postGenStatResult(List<ExecutionPayload> executionPayloads) throws IOException {
		String thurl = String.format("%ssegnodestat/genstatqueryresults", getWebServiceUrl());
		URL url = new URL(thurl);
		URLConnection conn;
		conn = url.openConnection();
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("X-Authorization", "token");
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		ObjectMapper mapper = new ObjectMapper();
		String payload = mapper.writeValueAsString(executionPayloads);
		//System.out.println("postGenerateDataResult payload:"
		//		+ "{ \"token\":\"82C36AF3-B220-459B-9AAD-88A81883E970\" , \"messages\": " + payload + "}");
		wr.write(payload);
		wr.write("{ \"token\":\"82C36AF3-B220-459B-9AAD-88A81883E970\" , \"messages\": " + payload + "}");
		wr.flush();
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		wr.close();
		rd.close();
	}
	
	public List<ExecutionPayload> getInsertQuery(String token, String execid) throws IOException {
		String url = String.format("%ssegmentmodelref/get-insertsqlqueries?token=%s&execid=%s", getWebServiceUrl(), token, execid);
		_logger.info("url = " + url);
		URL myURL = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
		conn.setRequestProperty("X-Authorization", token);
		conn.setRequestMethod("GET");
		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		ObjectMapper mapper = new ObjectMapper();
		
		List<ExecutionPayload> executionPayloads = mapper.readValue(rd,
				mapper.getTypeFactory().constructCollectionType(List.class, ExecutionPayload.class));
		rd.close();
		return executionPayloads;
	}
	
	public void postExecutedQueryStatus(List<ExecutionPayload> executionPayloads) throws IOException {
		String thurl = String.format("%ssegmentmodelref/insertquerystatus", getWebServiceUrl());
		URL url = new URL(thurl);
		URLConnection conn;
		conn = url.openConnection();
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("X-Authorization", "token");
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(executionPayloads);
		String payload = mapper.writeValueAsString(executionPayloads);
		System.out.println("payload:"
				+ "{ \"token\":\"82C36AF3-B220-459B-9AAD-88A81883E970\" , \"messages\": " + payload + "}");
		wr.write(payload);
		wr.write("{ \"token\":\"82C36AF3-B220-459B-9AAD-88A81883E970\" , \"messages\": " + payload + "}");
		wr.flush();
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		wr.close();
		rd.close();
	}
	
	public String getWebServiceUrl() {
		return webServiceUrl;
	}

	public void setWebServiceUrl(String webServiceUrl) {
		this.webServiceUrl = webServiceUrl;
	}
	
}