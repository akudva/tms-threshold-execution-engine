package com.ey.digital.adc.aml.exec.engine.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ey.digital.adc.aml.exec.engine.service.WebServiceClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import org.springframework.stereotype.Component;

import com.ey.digital.adc.aml.exec.engine.model.ExecutionPayload;
//import com.ey.digital.adc.aml.exec.engine.model.ExecutionPayload;
import com.ey.digital.adc.aml.exec.engine.service.RuleExecutionService;
import com.ey.digital.adc.aml.exec.engine.service.WebServiceClient;

import java.io.IOException;

import java.util.*;

@Component
public class LaunchRuleExecution {
	private final static Logger _logger = LoggerFactory.getLogger(LaunchRuleExecution.class.getName());
	@Autowired
	@Qualifier("webServiceClientSegmentation")
	WebServiceClient webServiceClientSegmentation;

	@Autowired
	@Qualifier("webServiceClientBackend")
	WebServiceClient webServiceClientBackend;
	@Autowired
	RuleExecutionService ruleExecutionService;

	public static void main(String[] args) throws IOException {

		String actionType = System.getenv("action");
		System.out.println("action = " + actionType);

		if (actionType.equalsIgnoreCase("RE")) {
			String token = System.getenv("token");
			Integer executeId = Integer.parseInt(System.getenv("texecid").replace("X", ""));

			AbstractApplicationContext context = new ClassPathXmlApplicationContext(
					"/META-INF/spring/exec-engine-context.xml", LaunchRuleExecution.class);
			_logger.info("Rule Execution engine Running");
			context.registerShutdownHook();
			LaunchRuleExecution launchRuleExecution = (LaunchRuleExecution) context.getBean("launchRuleExecution");
			ExecutionPayload executionPayload = launchRuleExecution.webServiceClientBackend
					.getExecutableRulesHeader(token, executeId);
			List<String> sqlQueries = executionPayload.getSqlQueries();

			String last = executionPayload.getSqlQueries().remove(executionPayload.getSqlQueries().size() - 1);

			for (String sqlQuery : sqlQueries) {
				System.out.println(sqlQuery + ";");
				launchRuleExecution.ruleExecutionService.execute(sqlQuery);
			}
			Integer result = launchRuleExecution.ruleExecutionService.executeForResult(last);
			executionPayload.setGetResult(true);
			executionPayload.setResultData(new HashMap<>());
			executionPayload.getResultData().put("result", Arrays.asList(result.toString()));
			launchRuleExecution.webServiceClientBackend.postResult(executionPayload);
			context.close();
			_logger.info("Rule Execution Completed");
		}
		if (actionType != null && actionType.equalsIgnoreCase("GD")) {
			String token = System.getenv("token");
			Integer ruleid = Integer.parseInt(System.getenv("ruleid"));
			String texecid = System.getenv("texecid");
			Integer rtcid = Integer.parseInt(System.getenv("rtcid"));

			// String token = "82C36AF3-B220-459B-9AAD-88A81883E970";
			// Integer ruleid = 2931;
			// String texecid = "TEXEC_568";
			// Integer rtcid = 2412;

			_logger.info("rtcId = " + rtcid);
			_logger.info("ruleId = " + ruleid);
			_logger.info("token = " + token);
			_logger.info("texecid = " + texecid);

			AbstractApplicationContext context = new ClassPathXmlApplicationContext(
					"/META-INF/spring/exec-engine-context.xml", LaunchRuleExecution.class);
			_logger.info("Generate Data engine Running");
			context.registerShutdownHook();
			LaunchRuleExecution launchRuleExecution = (LaunchRuleExecution) context.getBean("launchRuleExecution");
			List<ExecutionPayload> executionPayloads = launchRuleExecution.webServiceClientBackend
					.getExecutableGenerateDataHeader(token, ruleid, rtcid, texecid);
			List<ExecutionPayload> returnExecutionPaylds = new ArrayList<ExecutionPayload>();
			for (ExecutionPayload exPayLoad : executionPayloads) {
				// returnExecutionPaylds.add(exPayLoad);
				_logger.info("execution payload --each >>>>" + exPayLoad);
				List<String> sqlQueries = exPayLoad.getSqlQueries();
				// int x = 1;
				for (String sqlQuery : sqlQueries) {
					_logger.info("sqlQuery = " + sqlQuery);
					if (sqlQuery != null) {
						// String sqlqryrepace = sqlQuery.replaceAll(";", "");
						// System.out.println("after deleting semi colon:"+ sqlqryrepace);
						if (exPayLoad.isGetResult()) {
							Map<String, List<String>> results = launchRuleExecution.ruleExecutionService
									.executeQry(sqlQuery);

							ExecutionPayload executionPayload = new ExecutionPayload(exPayLoad);
							executionPayload.setResultData(results);
							returnExecutionPaylds.add(executionPayload);
							// System.out.println("pay load result size:"+ returnExecutionPaylds.size());
							// launchRuleExecution.webServiceClient.postGenerateDataResult(returnExecutionPaylds);
						} else {
							launchRuleExecution.ruleExecutionService.executeQuery(sqlQuery);
						}
					}
				}

			}
			_logger.info("pay load result size:" + returnExecutionPaylds.size());
			launchRuleExecution.webServiceClientBackend.postGenerateDataResult(returnExecutionPaylds);

			context.close();
			_logger.info("Threshold Execution Generate Data Completed");
		}

		if (actionType != null && actionType.equalsIgnoreCase("CM")) {
			String token = System.getenv("token");
			Integer ruleId = Integer.parseInt(System.getenv("ruleid"));
			String executeId = String.valueOf(System.getenv("texecid"));
			Integer rtcId = Integer.parseInt(System.getenv("rtcid"));
			_logger.info("rtcId = " + rtcId);
			_logger.info("ruleId = " + ruleId);
			_logger.info("token = " + token);

			// Testing values
			// String token = "82C36AF3-B220-459B-9AAD-88A81883E970";
			// Integer ruleid = 2931;
			// String texecid = "TEXEC_192";
			// Integer rtcId = 2411;

			AbstractApplicationContext context = new ClassPathXmlApplicationContext(
					"/META-INF/spring/exec-engine-context.xml", LaunchRuleExecution.class);
			_logger.info("Rule Execution engine Running");
			context.registerShutdownHook();
			LaunchRuleExecution launchRuleExecution = (LaunchRuleExecution) context.getBean("launchRuleExecution");

			List<ExecutionPayload> executionPayloads = launchRuleExecution.webServiceClientBackend
					.getExecutableThresholdCommitHeader(token, rtcId);

			for (ExecutionPayload exPayLoad : executionPayloads) {
				if (exPayLoad.isGetResult()) {
					List<String> sqlQueries = exPayLoad.getSqlQueries();
					List<String> resultList = new ArrayList<>();
					Map<String, List<String>> resultMap = new HashMap<>();

					for (String sqlQuery : sqlQueries) {
						_logger.info("sqlQuery = " + sqlQuery);
						String queryResult = launchRuleExecution.ruleExecutionService.getValue(sqlQuery).toString();
						resultList.add(queryResult);
						resultMap.put(exPayLoad.getId().toString(), resultList);
					}
					exPayLoad.setResultData(resultMap);
				}
			}

			launchRuleExecution.webServiceClientBackend.postThresholdCommitResult(executionPayloads);
			context.close();
			_logger.info("Thresholds Commit Completed");
		}

		if (actionType != null && actionType.equalsIgnoreCase("SG")) {
			String token = System.getenv("token");
			Integer modelid = Integer.parseInt(System.getenv("modelid"));
			// String token = "82C36AF3-B220-459B-9AAD-88A81883E970";
			// Integer modelid = 29;
			_logger.info("modelid = " + modelid);
			_logger.info("token = " + token);

			AbstractApplicationContext context = new ClassPathXmlApplicationContext(
					"/META-INF/spring/exec-engine-context.xml", LaunchRuleExecution.class);
			_logger.info("Segmentation Generate Data engine Running");
			context.registerShutdownHook();
			LaunchRuleExecution launchRuleExecution = (LaunchRuleExecution) context.getBean("launchRuleExecution");
			List<ExecutionPayload> executionPayloads = launchRuleExecution.webServiceClientSegmentation
					.getSegGenerateDataHeader(token, modelid);
			List<ExecutionPayload> returnExecutionPaylds = new ArrayList<ExecutionPayload>();
			for (ExecutionPayload exPayLoad : executionPayloads) {
				// returnExecutionPaylds.add(exPayLoad);
				_logger.info("execution payload --each >>>>" + exPayLoad);
				List<String> sqlQueries = exPayLoad.getSqlQueries();
				// int x = 1;
				for (String sqlQuery : sqlQueries) {
					_logger.info("sqlQuery = " + sqlQuery);
					if (sqlQuery != null) {
						// String sqlqryrepace = sqlQuery.replaceAll(";", "");
						// System.out.println("after deleting semi colon:"+ sqlqryrepace);
						if (exPayLoad.isGetResult()) {
							if ("numeric".equalsIgnoreCase(exPayLoad.getType())) {
								Map<String, List<String>> results = launchRuleExecution.ruleExecutionService
										.executeNumericQry(sqlQuery);

								ExecutionPayload executionPayload = new ExecutionPayload(exPayLoad);
								executionPayload.setResultData(results);
								returnExecutionPaylds.add(executionPayload);
							} else if ("string".equalsIgnoreCase(exPayLoad.getType())) {
								Map<String, List<String>> results = launchRuleExecution.ruleExecutionService
										.executeCategoricalQry(sqlQuery);

								ExecutionPayload executionPayload = new ExecutionPayload(exPayLoad);
								executionPayload.setResultData(results);
								returnExecutionPaylds.add(executionPayload);

							}
							// System.out.println("pay load result size:"+ returnExecutionPaylds.size());
							// launchRuleExecution.webServiceClient.postGenerateDataResult(returnExecutionPaylds);
						} else {
							launchRuleExecution.ruleExecutionService.executeQuery(sqlQuery);
						}
					}
				}

			}
			_logger.info("Seg GD - pay load result size:" + returnExecutionPaylds.size());
			launchRuleExecution.webServiceClientSegmentation.postSGGenerateDataResult(returnExecutionPaylds);

			context.close();
			_logger.info("Segmentation Generate Data Completed");
		}

		if (actionType != null && actionType.equalsIgnoreCase("GS")) {
			// String token = "82C36AF3-B220-459B-9AAD-88A81883E970";
			// String execid = "EXEC_641";
			String token = System.getenv("token");
			String execid = String.valueOf(System.getenv("texecid"));
			_logger.info("execid = " + execid);
			_logger.info("token = " + token);

			AbstractApplicationContext context = new ClassPathXmlApplicationContext(
					"/META-INF/spring/exec-engine-context.xml", LaunchRuleExecution.class);
			_logger.info("Generate Stats Running");
			context.registerShutdownHook();
			LaunchRuleExecution launchRuleExecution = (LaunchRuleExecution) context.getBean("launchRuleExecution");

			List<ExecutionPayload> executionPayloads = launchRuleExecution.webServiceClientSegmentation
					.getGenStats(token, execid);
			List<ExecutionPayload> returnExecutionPaylds = new ArrayList<ExecutionPayload>();

			for (ExecutionPayload exPayLoad : executionPayloads) {
				if (exPayLoad.isGetResult()) {
					ExecutionPayload executionPayload = new ExecutionPayload(exPayLoad);
					List<String> sqlQueries = exPayLoad.getSqlQueries();
					List<String> resultList = new ArrayList<>();
					Map<String, List<String>> resultMap = new HashMap<>();
					List<Double> result = new ArrayList<>();
					for (int i = 0; i < sqlQueries.size(); i++) {
						String sqlQuery = sqlQueries.get(i);
						System.out.println();
						if (sqlQuery != null && !sqlQuery.isEmpty()) {
							Double midResult = launchRuleExecution.ruleExecutionService.execQryForGenStats(sqlQuery)
									/ 1.00;
							if (result.size() > 0) {
								if (midResult == 0) {
									result.add(0.0);
								} else {
									result.add((result.get(0) / midResult) * 100);
								}
							} else {
								result.add(midResult);
							}
							resultList.add(result.get(i).toString());

						}
					}
					resultMap.put(exPayLoad.getId().toString(), resultList);
					returnExecutionPaylds.add(executionPayload);
					executionPayload.setResultData(resultMap);
				}
			}
			_logger.info("pay load result size:" + returnExecutionPaylds.size());
			System.out.println(returnExecutionPaylds);
			System.out.println("pay load result size:" + returnExecutionPaylds.size());
			launchRuleExecution.webServiceClientSegmentation.postGenStatResult(returnExecutionPaylds);
			context.close();
			_logger.info("Stats successfully generated");
		}

		// 11-08-2017 : AML-932 : Populates modelsegmentfocalentityref table
		// e.g. modelsegmentpartyref / modelsegmentaccountref
		// action for Focal Entity Ref Model Insert Hive Queries

		if (actionType != null && actionType.equalsIgnoreCase("FR")) {
//			 String token = "82C36AF3-B220-459B-9AAD-88A81883E970";
//			 String execid = "EXEC_1470";
			
			String token = System.getenv("token");
			String execid = String.valueOf(System.getenv("texecid"));
			_logger.info("execid = " + execid);
			_logger.info("token = " + token);
			AbstractApplicationContext context = new ClassPathXmlApplicationContext(
					"/META-INF/spring/exec-engine-context.xml", LaunchRuleExecution.class);
			_logger.info("Insert into Focal entity ref table is running");
			context.registerShutdownHook();
			LaunchRuleExecution launchRuleExecution = (LaunchRuleExecution) context.getBean("launchRuleExecution");

			List<ExecutionPayload> executionPayloads = launchRuleExecution.webServiceClientSegmentation
					.getInsertQuery(token, execid);
			System.out.print(executionPayloads);
			for (ExecutionPayload exPayLoad : executionPayloads) {
				List<String> sqlQueries = exPayLoad.getSqlQueries();
				for (String sql : sqlQueries) {
					launchRuleExecution.ruleExecutionService.executeQuery(sql);
				}
			}
			_logger.info("focal entity model ref  Insert query successfully executed");
			// Returning original execution payload
			launchRuleExecution.webServiceClientSegmentation.postExecutedQueryStatus(executionPayloads);
			context.close();
		}
	}
}